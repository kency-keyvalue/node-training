const uuid = require('uuid').v4;
const employees = [];

exports.getEmployees = (req, res) => {
    res.send(employees);
};

exports.createEmployee = (req, res) => {
  console.log(JSON.stringify(req.body));
  const employee = { ...req.body };
  employee.id = uuid();
  employees.push(employee);
  res.json(employee);
};