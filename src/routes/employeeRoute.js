const express = require('express');
const validate = require('express-validation');
const { createEmployee } = require('../validations');
const employeeController = require('../controllers/employeeController');

const router = express.Router();

router.get('/', employeeController.getEmployees);

router.post('/', validate(createEmployee), employeeController.createEmployee)

module.exports = router;